let myApp = angular.module('FormulaCalc', []);

myApp.controller('FormulaController', function FormulaController($scope) {
    let self = this;

    self.calculateFormula = calculateFormula;
    self.showHelp = showHelp;

    self.selectedFormula = '';
    self.aArgument = 0;
    self.bArgument = 0;
    self.cArgument = 0;
    self.showArgumentForm = false;
    self.arguments = [];
    self.filledArguments = [];
    self.results = [];
    self.max = 100000000;
    self.min = -100000000;

    self.formulas = [
        {
            name: 'y = e^x',
            arguments: [],
            calculate: (xArgument) => {
                return Math.pow(Math.E, xArgument);
            }
        },
        {
            name: 'y = a^x',
            arguments: ['a'],
            calculate: (xArgument) => {
                return Math.pow(self.filledArguments[0], xArgument);
            }
        },
        {
            name: 'y = a * x + b',
            arguments: ['a', 'b'],
            calculate: (xArgument) => {
                return self.filledArguments[0] * xArgument + +self.filledArguments[1];
            }
        },
        {
            name: 'y = a * x^2 + b * x + c',
            arguments: ['a', 'b', 'c'],
            calculate: (xArgument) => {
                return self.filledArguments[0] * Math.pow(xArgument, 2) +
                    self.filledArguments[1] * xArgument + self.filledArguments[2];
            }
        },
        {
            name: 'y = x * sin a',
            arguments: ['a'],
            calculate: (xArgument) => {
                return  Math.sin(Math.PI * self.filledArguments[0]) * xArgument;
            }
        },
        {
            name: 'y = x * tg a',
            arguments: ['a'],
            calculate: (xArgument) => {
                return  Math.tan(Math.PI * self.filledArguments[0]) * xArgument;
            }
        }
    ];

    $scope.$watch(() => self.selectedFormula,
        function onFormulaChange(newValue, oldValue) {
            if (newValue !== oldValue) {
                self.showArgumentForm = true;
                self.arguments = getSelectedFormula().arguments;
            }
        });

    function calculateFormula() {
        try {
        let index,
            formula;
        if ((self.aArgument || self.aArgument === 0) &&
            (self.bArgument || self.bArgument === 0)) {
            for (let index = 0; index < self.arguments.length; index++) {
                if (!self.filledArguments[index]) {
                    self.filledArguments[index] = 0;
                }
            }
            self.results = [];

            formula = getSelectedFormula();

            for (index = -10; index <= 10; index++) {
                self.results.push(
                    formula.calculate(index)
                );
            }
            if (self.results.some((res) => res > self.max)  || self.results.some((res) => res < self.min)) {
                self.results = [];
                throw Error('Слишком большой или слишком маленький результат для отображения \n' +
                    'скорее всего, вы ввели некорректные аргументы');
            }
        }
        } catch(error) {
            alert('Пожалуйста, свяжитесь с администратором и сообщите ему о следующей ошибке: \n' + error);
        }
    }



    function getSelectedFormula() {
        function isSelected(element) {
            return element.name === self.selectedFormula;
        }

        return self.formulas.find(isSelected);
    }
    
    function showHelp() {
        alert('Выберите одну из формул, заполните поля с аргументами \n' +
            'Нажмите кнопку "Отправить" \n' +
            'В случае если поле не заполнено, его значение будет "0" \n' +
            'В случае, когда выбрана функция с тригонометрическими операциями, ' +
            'аргументы будут считаться в долях от Пи например "sin a*pi" вместо "sin a" \n' +
            'Результаты высчитываются в пределах от -100000000 до 100000000 \n' +
            'в противном случае система выдаст ошибку');
    }
});